 <?php
    use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Batch;
    require('vendor/autoload.php');

    $client = new Everyman\Neo4j\Client('localhost', 7474);
    $values = new NodeIndex($client, 'values');
    $a = array_fill(0,100,0);
    print_r($a);
    for($i=1;$i<=10;$i++) {
	
	if($a[$i]==0) {

		$parent = $client->makeNode()
		->setProperty('value',$i)
		->save();
		$values->add($parent, 'value', $parent->getProperty('value'));
		$a[$i]=$parent->getId();
	}
	
	else {
		$parent=$client->getNode($a[$i]);
	}
	
	for($j=2;$j<=10;$j++) {

	   if($a[$i*$j]==0) {

		$child = $client->makeNode()
		->setProperty('value',$i*$j)
		->save();
		$values->add($child, 'value', $child->getProperty('value'));
		$a[$i*$j]=$child->getId();
	   }
		
	   else {
		$child=$client->getNode($a[$i*$j]);
	   }
	

	 $divisor = $parent->relateTo($child, 'divisorOf')
		->save();
	 $divisor = $child->relateTo($parent, 'multipleOf')
		->save();
	
	}
	
    }

    $values->save();
    echo $values->queryOne('value:3')->getProperty('value') . "\n";
?>
