 <?php
    use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Index\NodeFulltextIndex,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Batch;
    require('vendor/autoload.php');
    echo "client created" ."</br>" ;
    $client = new Everyman\Neo4j\Client('localhost', 7474);
    $values = new NodeIndex($client, 'values');
    $startNode=$values->queryOne('value:3');
    echo  "start node -> " .  $startNode->getProperty("value")." & ". $startNode->getId() ."</br>";

    $endNode=$values->queryOne('value:27');
    echo  "end node -> " .  $endNode->getProperty("value")." & ". $endNode->getId() ."</br>";

	$path = $startNode->findPathsTo($endNode)
    		->getPaths();
    echo "here" ;
    echo "The path is " . count($path) . "relationships long\n";
  	foreach ($path as $node) {
    		echo $node->getId() . "\n";
	}

echo "hi";
