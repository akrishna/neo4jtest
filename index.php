 <?php
    require('vendor/autoload.php');

    $client = new Everyman\Neo4j\Client('localhost', 7474);
    $arthur = $client->makeNode();
    $arthur->setProperty('name', 'Ankur Gautam')
    ->setProperty('mood', 'nervous')
    ->setProperty('home', 'IIT BHU')
    ->save();

    print_r($arthur->getId());

?>
